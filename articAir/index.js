window.onload = function(e) {
  alert("Main Desktop 1920");
  function scrollAnimation() {
    let firstP = document.getElementById("firstP");
    let secondP = document.getElementById("secondP");
    let thirdP = document.getElementById("thirdP");
    let fourthP = document.getElementById("fourthP");
    let fifthP = document.getElementById("fifthP");
    let sixthP = document.getElementById("sixthP");
    if (scrollY < 500) {
      firstP.style.color = "#005799";
      secondP.style.color = "#6CC0F2";
      thirdP.style.color = "#6CC0F2";
    }
    if (scrollY > 500 && scrollY < 1200) {
      secondP.style.color = "#005799";
      firstP.style.color = "#6CC0F2";
      thirdP.style.color = "#6CC0F2";
    }
    if (scrollY > 1200) {
      thirdP.style.color = "#005799";
      secondP.style.color = "#6CC0F2";
      firstP.style.color = "#6CC0F2";
    }
  }

  //
  scrollAnimation();
  document.onscroll = function(e) {
    console.log(scrollY);
    scrollAnimation();
  };
  //

  $("#firstP").click(function(e) {
    e.preventDefault();

    $("body,html").animate(
      {
        scrollTop: 0
      },
      500
    );
  });
  $("#secondP").click(function(e) {
    e.preventDefault();

    $("body,html").animate(
      {
        scrollTop: 1000
      },
      500
    );
  });
  $("#thirdP").click(function(e) {
    e.preventDefault();

    $("body,html").animate(
      {
        scrollTop: 1700
      },
      500
    );
  });
};
